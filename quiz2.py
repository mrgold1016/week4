#Griffin Moran
#September 22, 2015
#quiz2.py
#start()
#Task 1
def task1():
  z = pickAFile()
  picture = makePicture(z)
  pixels = getPixels(picture)
  h = getHeight(picture)
  w = getWidth(picture)
  show(picture)
  for x in range(0,w):
    for y in range(0,h):
      pixel = getPixelAt(picture,x,y)
      b = getBlue(pixel)
      if(b <= 149):
        setBlue(pixel, 255)
        setRed(pixel, 255)
        setGreen(pixel, 255)
  repaint(picture)
#task1()

def task2():
  z = pickAFile()
  picture = makePicture(z)
  pixels = getPixels(picture)
  h = getHeight(picture)
  w = getWidth(picture)
  show(picture)
  for x in range(0,w):
    for y in range(0,h):
      pixel = getPixelAt(picture,x,y)
      b = getBlue(pixel)
      r = getRed(pixel)
      g = getGreen(pixel)
      setBlue(pixel, b*2 )
      setRed(pixel, r/2)
      setGreen(pixel, g/2)
  repaint(picture)      
#task2()

def task3():
  z = pickAFile()
  picture = makePicture(z)
  pixels = getPixels(picture)
  h = getHeight(picture)
  w = getWidth(picture)
  show(picture)
  for x in range(0,w):
    for y in range(0,h):
      pixel = getPixelAt(picture,x,y)
      b = getBlue(pixel)
      r = getRed(pixel)
      g = getGreen(pixel)
      setBlue(pixel, b/2 )
      setRed(pixel, r*2)
      setGreen(pixel, g/2)
  repaint(picture)      
#task3()   
def task4():
  def greyScaleAndNegate(pic):  

     for px in getPixels(pic):
        level = 255 - int(0.21*getRed(px) + 0.71*getGreen(px) +0.07*getBlue(px))
        color = makeColor(level, level, level)
        setColor(px, color)


  file = pickAFile()
  picture = makePicture(file) 
  greyScaleAndNegate(picture)
  show(picture)
#task4()

#q = pickAFile()
#picture = makePicture(q)
def aFunction(picture):
 xmax = getWidth(picture)
 ymax = getHeight(picture)
 
 for x in range(0,xmax):
   for y in range(0,ymax):
     pixel = getPixelAt(picture,x,y)
     b = getBlue(pixel)
     r = getRed(pixel)
     g = getGreen(pixel)
     newVal = ((r+g+b) / 3) + 75
     r = g = b = newVal
     color = makeColor(r,g,b)
     setColor(pixel, color)
 repaint(picture)
 show(picture)
 
#aFunction(picture)


def task6():
 p = pickAFile()
 pict = makePicture(p)
 pixels = getPixels(pict)
 h = getHeight(pict)
 q = h/2
 w = getWidth(pict)
 for x in range(0,w):
   for y in range(0,q):
     value = getPixelAt(pict,x,y)
     b = getBlue(value)
     r = getRed(value)
     g = getGreen(value)
     setBlue(value, 0)
     setGreen(value, 0)
     setRed(value, 0)
 repaint(pict)
 show(pict)    
 
#task6()

def mirrorHalf():
 p = pickAFile()
 pict = makePicture(p)
 pixels = getPixels(pict)
 h = getHeight(pict)
 w = getWidth(pict)
 for y in range(0,h/2):
   for x in range(0,w):
     p = getPixel(pict, x, y)
     z = getPixel(pict, x, h - 1 - y)
     color = getColor(p)
     setColor(z, color)

   repaint(pict)
   show(pict)
mirrorHalf()
